@extends('layouts.app')

@section('title', 'Productos')

@section('content')
<style type="text/css">
.alert {
  visibility: hidden;
  padding: 5px;
  background-color: #faa; /* Red */
  margin: 5px;
}
</style>
<h1>Alta de producto</h1>
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
    {{--
        --}}

        @if ($errors->any())
        <div class="alert alert-danger">
            Se han producido errores de validación
        </div>
        @endif

        <form method="post" action="/products">
            {{ csrf_field() }}

            <label>Nombre</label>
            <input type="text" name="name" value="{{ old('name') }}">
            <div class="alert alert-danger">
                {{ $errors->first('name') }}
            </div>
            <br>

            <label>Precio</label>
            <input type="text" name="price" value="{{ old('price') }}">
            <div class="alert alert-danger">
                {{ $errors->first('price') }}
            </div>

            <br>

            <label>Categoría</label>
            <select name="cathegory_id">
                @foreach ($categorias as $categoria)
                <option value="{{ $categoria->id }}"
                {{ old('cathegory_id') == $categoria ?
                'selected="selected"' :
                ''
            }}>{{ $categoria->name }}
        </option>
        @endforeach
    </select>
    <br>

    @if(!count($categorias) == 0) {{-- Si no hay categorías creadas --}}
    <input type="submit" value="Crear">
    @else
     <div class="alert alert-danger">
                No hay categorías. Crea una <a href="/cathegories/create"> Aquí </a>
    </div>
    @endif

</form>
@endsection
