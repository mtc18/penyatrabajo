@extends('layouts.app')

@section('content')
<h1>Lista de Productos</h1>

<ul>
    <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Categoría</th>
        </tr>
    </thead>
    <tbody>
      @forelse ($products as $product)
      <tr>
        <td>{{ $product->id }}</td>
        <td>{{ $product->name }}</td>
        <td>{{ $product->price }}</td>
        <td>{{ $product->cathegory->name }}</td>
        <td>

          <form method="post" action="/products/{{ $product->id }}">
            @can('edit', $product)
            <a class="btn btn-primary"  role="button"
            href="/products/{{ $product->id }}/edit">
            Editar
        </a>
        @endcan
        <a class="btn btn-primary"  role="button"
        href="/products/{{ $product->id }}">
        Ver
    </a>
    {{ csrf_field() }}
    @can('delete', $product)
    <input type="hidden" name="_method" value="DELETE">
    <input type="submit" value="Borrar" class="btn btn-primary">
@endcan
</form>
</td>
</tr>
@empty
<tr><td colspan="4"><strong>No hay Productos</strong></td></tr>
@endforelse
</tbody>
</table>
@can('create', App\Product::class)
<a href="/products/create" class="btn btn-primary">Nuevo</a>
@endcan
{{ $products->render() }}
</div>
</div>
</div>
@endsection
